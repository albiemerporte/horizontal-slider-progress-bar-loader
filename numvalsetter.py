
from PyQt6.uic import loadUi
from PyQt6.QtWidgets import QApplication

class NumValSetterProg:
    def __init__(self):
        self.mainui = loadUi('numvalsetter.ui')
        self.mainui.show()
        self.mainui.barMeasure.setValue(0)
        
        for s in range(1,6):
            slidersetr = getattr(self.mainui, f'hsliderVal{s}')
            slidersetr.setRange(0, 100)
        
        for c in range(1,6):
            sliderconnect = getattr(self.mainui, f'hsliderVal{c}')
            sliderconnect.valueChanged.connect(self.update_progress)
        
    def update_progress(self):
        
        value1 = self.mainui.hsliderVal1.value()
        value2 = self.mainui.hsliderVal2.value()
        value3 = self.mainui.hsliderVal3.value()
        value4 = self.mainui.hsliderVal4.value()
        value5 = self.mainui.hsliderVal5.value()

        self.mainui.lblVal1.setText(str(value1))
        self.mainui.lblVal2.setText(str(value2))
        self.mainui.lblVal3.setText(str(value3))
        self.mainui.lblVal4.setText(str(value4))
        self.mainui.lblVal5.setText(str(value5))

        alltask = sum([int(value1), \
                       int(value2), \
                       int(value3), \
                       int(value4), \
                       int(value5)])
        
        tasknum = 500
        
        progress_status = (alltask / tasknum) * 100
        self.mainui.barMeasure.setValue(int(progress_status))
        self.mainui.lblAccurate.setText(f'Accurate Number: {progress_status}')

if __name__ == '__main__':
    app = QApplication([])
    main = NumValSetterProg()
    app.exec()